const express = require('express');
const http = require('http');
const cors = require('cors');
const socketio = require('socket.io');

const tasksRoute = require('./routes/tasks');

const app = express();
const server = http.createServer(app);
const io = socketio(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"]
  }
});

app.use(cors());
app.use(express.json());
app.io = io;

app.use('/api/v1/tasks', tasksRoute)

io.on('connection', socket => {
  console.log('A client is connected to socket server!');

  socket.on('disconnext', () => {
    console.log('A client is disconnected from socket server!')
  })
})

const PORT = process.env.PORT || 5000;

server.listen(PORT, () => console.log(`Server is running on PORT: ${PORT}`))