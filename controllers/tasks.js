const tasks = [
  {
    id: 1,
    title: 'Task 1',
    description: "Description 1",
    status: "to_do",
    created_at: new Date()
  }
];

const getTasks = (req, res) => {
  req.app.io.emit('info', 'a client is fetching tasks');
  res.json(tasks);
}

const getFilterTasks = (req, res) => {
  const { status } = req.query;

  const filtered_tasks = tasks.filter(task => task.status == status);
  req.app.io.emit('info', 'a client is fetching filtered tasks');
  res.json(filtered_tasks)
}

const createTask = (req, res) => {
  const { title, description, status } = req.body;

  const newTask = {
    id: `${(Math.random() * 10000000) + 1}`,
    title,
    description,
    status: status,
    created_at: new Date()
  }

  tasks.push(newTask);

  req.app.io.emit('task_created', newTask);
  res.json(newTask)
}

const updateTask = (req, res) => {
  const { id } = req.params;
  const { title, description, status } = req.body;

  const updateField = {};

  if (title) {
    updateField.title = title;
  }

  if (description) {
    updateField.description = description;
  }

  if (status) {
    updateField.status = status;
  }

  const selectedTask = tasks.find(task => task.id == id);
  const selectedIndex = tasks.indexOf(selectedTask);
  tasks[selectedIndex] = {
    ...selectedTask,
    ...updateField
  }

  req.app.io.emit('task_updated', tasks[selectedIndex]);
  res.json(tasks[selectedIndex])
}

module.exports = { getTasks, getFilterTasks, createTask, updateTask }