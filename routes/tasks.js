const express = require('express');

const { getTasks, getFilterTasks, createTask, updateTask } = require('../controllers/tasks');

const router = express.Router();

router.get('/', getTasks);
router.get('/filter', getFilterTasks);
router.post('/:id', updateTask);
router.post('/', createTask);

module.exports = router;